import Vue from "vue";
import Router from "vue-router";
import TodosList from "./components/TodosList.vue";
import AddTodo from "./components/AddTodo.vue";
import SearchTodos from "./components/SearchTodos.vue";
import Todo from "./components/Todo.vue";
import UpdateTodo from "./components/UpdateTodo.vue"
import Login from './components/auth/Login'
import Logout from './components/auth/Logout'
import Register from './components/auth/Register'
 
Vue.use(Router);
 
export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Todos",
      alias: "/Todo",
      component: TodosList
    },
    {
      path: "/add",
      name: "add",
      component: AddTodo
    },
    {
      path: "/search",
      name: "search",
      component: SearchTodos
    },
    {
      path: "/update",
      name: "update",
      component: UpdateTodo
    },
    {
      path: "*",
      redirect: "/"
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        requiresVisitor: true,
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        requiresVisitor: true,
      }
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout
    }
  ]
});