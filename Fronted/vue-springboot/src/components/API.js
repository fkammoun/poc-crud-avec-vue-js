import axios from "axios";
 
export default axios.create({
  baseURL: "http://localhost:9000/",
  headers: {
    "Content-type": "application/json",
    
  }
});


// //Add request interceptors
// axios.interceptors.request.use((config)=>{
//   let token = localStorage.getItem('authtoken');

//     if (token) {
//       config.headers['Authorization'] = `Bearer ${ token }`;
//     }
//   console.log('Request:',config);
//   return config;
// },
// (error)=>{
//   alert('il y a une erreur');
//   console.log(error);
// })

// //Add response interceptors
// axios.interceptors.response.use((response)=>{
//   console.log('response:',response);
//   return response;
// },
// (error)=>{
//   alert('il y a une erreur');
//   console.log(error);
// }
// )