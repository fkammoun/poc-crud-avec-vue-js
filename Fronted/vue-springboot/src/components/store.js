import Vue from 'vue'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
import http from "./API";
import axios from 'axios'
Vue.use(Vuex)
Vue.use(VueResource)

const state = {
  token: localStorage.getItem('TOKEN') || null ,
  todos: {},

}

const getters = {
  loggedIn(state) {
    return state.token !== null
  },
    selectedTodos: state => state.todos
}

const mutations = {
  SELECT_TODO: (state, todos) => {
    state.todos = todos
  },
  RETREIVE_TOKEN(state, aa) {
    state.token = aa
  },
  DESTROY_TOKEN(state) {
    state.token = null
  },
}

const actions = {

  selectTodoAction: (store, id) => {
    http.get(`/todo/${id}`).then((response) => {
      store.commit('SELECT_TODO', response.data)
      console.log(state)
    }, (response) => {
      console.log('erreur', response)
    })
  },

  registerAction(context, data) {
      http.post('/register', {
        name: data.name,
        mail: data.mail,
        password: data.password,
        roles:data.roles
      })
        .then(response => {
          console.log(response)
        })
        .catch(error => {   
    })
  },

  retrieveToken(context, credentials) {
    http.post('login', {
      mail: credentials.mail,
      password: credentials.password,
    })
      .then(response => {
        const token = response.headers.authorization.split(' ')[1]
        console.log(response)
        localStorage.setItem('TOKEN', token)
        context.commit('RETREIVE_TOKEN', token)
        console.log(token);
       console.log(response.config.data);
      context.commit('SELECT_TODO', response.config.data)
      })
      .catch(error => {
        console.log(error)
    })
},

  destroyToken(context) {
    http.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
    
    if (context.getters.loggedIn) {
     
        http.post('/logout')
          .then(response => {
            localStorage.removeItem('TOKEN')
            context.commit('DESTROY_TOKEN')
         
          })
          .catch(error => {
            localStorage.removeItem('TOKEN')
            context.commit('DESTROY_TOKEN')
           
          })
    }
  },
  
}

let store = new Vuex.Store({
  state: state,
  mutations: mutations,
  getters: getters,
  actions: actions,
  
})

export default store