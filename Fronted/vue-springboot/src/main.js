import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuex from 'vuex'
import store from './components/store'

Vue.use(Vuex)
Vue.config.productionTip = false

//Interceptors 

Vue.http.interceptors.push((request, next) => {
  
  if(localStorage.TOKEN) {
    request.headers.set(localStorage.TOKEN);
  }
  
  next((response) => {
    if(response.status == 401 || response.status == 403) {
      localStorage.removeItem('TOKEN');
      router.push({ name: 'login'});
    }
  });
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.loggedIn) {
      next({
        name: 'login',
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    if (store.getters.loggedIn) {
      next({
        name: 'Todos',
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
