package com.spring.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.todo.entity.todo;
@Repository
public interface todoRepository extends JpaRepository <todo, Integer> {

}
