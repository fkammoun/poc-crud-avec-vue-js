package com.spring.todo.security;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.spring.todo.entity.User;

import com.spring.todo.repository.UserRepository;

@Service
public class DbInit implements CommandLineRunner {
	private UserRepository userRepository;
	private PasswordEncoder passwordEncoder;

	public DbInit(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public void run(String... args) {
		// Delete all
		this.userRepository.deleteAll();

		// Crete users
		User farouk = new User(1, "farouk",passwordEncoder.encode("farouk123") , "farouk.kammoun@yahoo.fr", "USER");
		User admin = new User(2,"manef" , passwordEncoder.encode("manef123"),"manef.manef@yahoo.fr", "ADMIN");
		List<User> users = Arrays.asList(farouk, admin);
		

	
		// Save to db
		this.userRepository.saveAll(users);
	}
}
