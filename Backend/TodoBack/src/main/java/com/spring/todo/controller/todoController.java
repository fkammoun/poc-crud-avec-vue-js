package com.spring.todo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.todo.entity.todo;
import com.spring.todo.repository.todoRepository;

@RestController()
@CrossOrigin
public class todoController {

	@Autowired
	private todoRepository todoRepository;

	@PostMapping(path = "/todo", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody todo addTodo(@RequestBody todo newTodo) {
		return todoRepository.save(newTodo);
	}

	@PostMapping(path = "/updateTodo", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody void UpdateTodo(@RequestBody todo todo) {
		todoRepository.save(todo);
	}

	@GetMapping("/todo/{id}")
	public @ResponseBody todo findTodoById(@PathVariable int id) {
		if (todoRepository.findById(id).isPresent())
			return todoRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/todo")
	public @ResponseBody List<todo> findAllTodo() {
		return todoRepository.findAll();
	}

	@DeleteMapping(path = "/todo/{id}")
	public void deleteTodo(@PathVariable int id) {
		todoRepository.deleteById(id);
	}

}
