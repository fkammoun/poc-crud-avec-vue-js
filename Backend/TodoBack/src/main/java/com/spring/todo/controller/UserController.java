package com.spring.todo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.todo.entity.User;
import com.spring.todo.repository.UserRepository;

@RestController()
@CrossOrigin
public class UserController {
	
	@Autowired
	private PasswordEncoder pe;

	@Autowired
	private UserRepository userRepository;

	@PostMapping(path = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody User addUser(@RequestBody User newUser) {
		
		newUser.setPassword(pe.encode(newUser.getPassword()));
		return userRepository.save(newUser);
	}

	@GetMapping("/user")
	public @ResponseBody List<User> findAllTodo() {
		return userRepository.findAll();
	}
}
