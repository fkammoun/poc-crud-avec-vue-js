package com.spring.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.todo.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByMail(String mail);
	User findByName(String name);
}
